from flask import Flask
from flask import url_for, render_template, make_response, redirect
from markupsafe import escape # maybe i'm gonna use it in the future  example. f'{name}'

from cattery import subpages

def create_app():
    app = Flask(__name__)

    @app.route("/")
    def index():
        return make_response(render_template('index.html'))
    
    @app.errorhandler(404)
    def page_not_found(error):
        print("404 error occurred")
        return render_template('page_not_found.html'), 404
    
    app.register_blueprint(subpages.bp)
    #app.add_url_rule("/", endpoint="index")
    return app
