from flask import Blueprint
from flask import url_for, render_template, redirect

bp = Blueprint("subpage", __name__)


@bp.route("/o-nas")
def about_us():
    return render_template('about_us.html')


@bp.route("/kontakt")
def contact_us():
    return redirect(url_for('subpage.about_us'))


@bp.route("/mioty")
def litters():
    return render_template('litters.html')


@bp.route("/nasze-koty")
def our_cats():
    return render_template('our_cats.html')

# TODO
@bp.route("/wolne-kotki")
def available_kittens():
    return render_template('available_kittens.html')


# TODO
@bp.route("/fundacje")
def foundations():
    return render_template('foundations.html')
